Tetris
======

A classic Tetris game with following features:

 * background music (play/pause)
 * surfaceView
 * canvas
 * threads
 * listView, adapter (ViewHolder pattern)
 * shared preferences