package com.kitefiko.tetris.game;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.kitefiko.tetris.activity.GameActivity;

public class GameSurfaceView extends SurfaceView implements Runnable, OnGameAction {
    public GameBoard game;
    SurfaceHolder surfaceHolder;
    volatile boolean running = false;
    Thread thread = null;
    OnScoreSet onScoreSet;
    private Paint backgroundPaint;
    Activity activity;

    public GameSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        surfaceHolder = getHolder();

        setBackgroundColor(Color.TRANSPARENT);
        setZOrderOnTop(true);
        surfaceHolder.setFormat(PixelFormat.TRANSPARENT);

        backgroundPaint = new Paint();
        backgroundPaint.setColor(Color.argb(75, 0, 0, 0));

        game = new GameBoard(this);
    }

    public void setOnScoreSet(Activity activity, OnScoreSet onScoreSet) {
        this.activity = activity;
        this.onScoreSet = onScoreSet;
    }

    public int getScore() {
        return game.score;
    }

    public void onResumeGameSurfaceView() {
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public void onPauseGameSurfaceView() {
        boolean retry = true;
        running = false;
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void playPause() {
        if (running) {
            onPauseGameSurfaceView();
        } else {
            onResumeGameSurfaceView();
        }
    }

    @Override
    public void run() {
        while (running) {
            if (surfaceHolder.getSurface().isValid()) {
                Canvas canvas = surfaceHolder.lockCanvas();

                game.setSize(canvas.getWidth(), canvas.getHeight());

                canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), backgroundPaint);

                game.update();
                game.render(canvas);

                surfaceHolder.unlockCanvasAndPost(canvas);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onGameOver() {
        running = false;
        onScoreSet.onGameOver(game.score);
    }

    @Override
    public void onScoreUpsate(int score) {
        this.onScoreSet.setScore(score);
    }
}
