package com.kitefiko.tetris.game;

public interface OnScoreSet {
    void setScore(int score);
    void onGameOver(int score);
}
