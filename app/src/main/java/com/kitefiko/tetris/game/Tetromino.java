package com.kitefiko.tetris.game;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class Tetromino {

    private int x, y;
    private Paint pa;
    private int size;
    private List<Piece> pieces;


    Tetromino(Paint pa, int size, Piece... pieces) {
        this.pa = pa;
        this.size = size;
        this.pieces = new ArrayList<>(Arrays.asList(pieces));

        this.pieces.forEach((piece) -> piece.setParent(this));
    }

    int getX() {
        return this.x;
    }

    int getY() {
        return this.y;
    }

    List<Piece> getPieces() {
        return pieces;
    }

    void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;

        pieces.forEach(p -> {
            p.setX(p.getX() + dx);
            p.setY(p.getY() + dy);
        });
    }

    void move(Direction direction) {
        move(direction.x, direction.y);
    }

    void draw(Canvas canvas) {
        pieces.forEach(p -> canvas.drawRect(p.getX() * size, p.getY() * size, p.getX() * size + size, p.getY() * size + size, pa));
    }

    void rotateBack() {
        pieces.forEach(p -> p.setDirection(p.getDirection().prev().prev()));
    }

    void rotate() {
        pieces.forEach(p -> p.setDirection(p.getDirection().next().next()));
    }

    void detach(int x, int y) {
        pieces.removeIf(p -> p.getX() == x && p.getY() == y);
    }

    Tetromino copy() {
        return new Tetromino(pa, size, this.pieces.stream().map(Piece::copy).collect(Collectors.toList()).toArray(new Piece[0]));
    }
}

