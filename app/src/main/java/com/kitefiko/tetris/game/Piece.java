package com.kitefiko.tetris.game;

public class Piece {
    private int x, y;
    private int distance;
    private Direction direction;
    private Tetromino parent;

    public Piece(int distance, Direction direction) {
        this.distance = distance;
        this.direction = direction;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
        this.x = parent.getX() + distance * direction.x;
        this.y = parent.getY() + distance * direction.y;
    }

    public void setParent(Tetromino parent) {
        this.parent = parent;
        this.x = parent.getX() + distance * direction.x;
        this.y = parent.getY() + distance * direction.y;
    }

    public Piece copy() {
        return new Piece(distance, direction);
    }

}

