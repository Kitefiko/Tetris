package com.kitefiko.tetris.game;

interface OnGameAction {
    void onGameOver();
    void onScoreUpsate(int score);
}
