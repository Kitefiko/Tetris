package com.kitefiko.tetris.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

public class GameBoard {

    private Integer tileSize;
    private int GRID_WIDTH = 15;
    private int gridHeight;
    int score = 0;
    private OnGameAction onGameAction;

    private Tetromino selected;
    private ArrayList<Tetromino> original = new ArrayList<>();
    private ArrayList<Tetromino> tetrominos = new ArrayList<>();

    private int[][] grid;

    GameBoard(OnGameAction onGameAction) {
        this.onGameAction = onGameAction;
    }

    void setSize(int width, int height) {
        if (tileSize == null) {
            tileSize = width / GRID_WIDTH;
            gridHeight = height / tileSize;
            grid = new int[GRID_WIDTH][gridHeight];
            createTerminos();
        }
    }

    private boolean isValidState() {
        for (int y = 0; y < gridHeight; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                if (grid[x][y] > 1) {
                    return false;
                }
            }
        }

        return true;
    }

    private void sweep() throws IOException {
        List<Integer> rows = sweepRows();
        score += rows.size();
        onGameAction.onScoreUpsate(score);
        rows.forEach(row -> {
            for (int x = 0; x < GRID_WIDTH; x++) {
                for (Tetromino tetromino : tetrominos) {
                    tetromino.detach(x, row);
                }

                grid[x][row]--;
            }
        });

        rows.forEach(row -> {
            tetrominos.forEach(tetromino -> {
                tetromino.getPieces().stream()
                        .filter(piece -> piece.getY() < row)
                        .forEach(piece -> {
                            removePiece(piece);
                            piece.setY(piece.getY() + 1);
                            placePiece(piece);
                        });
            });
        });

        spawn();
    }

    private List<Integer> sweepRows() {
        List<Integer> rows = new ArrayList<>();

        outer:
        for (int y = 0; y < gridHeight; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                if (grid[x][y] != 1) {
                    continue outer;
                }
            }

            rows.add(y);
        }

        return rows;
    }

    private void placePiece(Piece piece) {
        grid[piece.getX()][piece.getY()]++;
    }

    private void removePiece(Piece piece) {
        grid[piece.getX()][piece.getY()]--;
    }

    private boolean isOffScreen(Piece piece) {
        return piece.getX() < 0 || piece.getX() >= GRID_WIDTH
                || piece.getY() < 0 || piece.getY() >= gridHeight;
    }

    private void makeMove(Consumer<Tetromino> onSuccess, Consumer<Tetromino> onFail, boolean endMove) throws IOException {
        selected.getPieces().forEach(this::removePiece);

        onSuccess.accept(selected);

        boolean offScreen = false;
        for (Piece p : selected.getPieces()) {
            if (isOffScreen(p)) {
                offScreen = true;
                break;
            }
        }


        if (!offScreen) {
            selected.getPieces().forEach(this::placePiece);
        } else {
            onFail.accept(selected);

            selected.getPieces().forEach(this::placePiece);

            if (endMove) {
                sweep();
            }

            return;
        }

        if (!isValidState()) {
            selected.getPieces().forEach(this::removePiece);

            onFail.accept(selected);

            selected.getPieces().forEach(this::placePiece);

            if (endMove) {
                sweep();
            }
        }
    }

    private void spawn() throws IOException {
        if (!isValidState()) {
            this.onGameAction.onGameOver();
        }

        Tetromino tetromino = original.get(new Random().nextInt(original.size())).copy();
        tetromino.move(GRID_WIDTH / 2, 2);
        selected = tetromino;
        tetrominos.add(tetromino);
        tetromino.getPieces().forEach(this::placePiece);
    }

    void update() {
        if (selected == null) {
            try {
                spawn();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                makeMove(p -> p.move(Direction.DOWN), p -> p.move(Direction.UP), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void render(Canvas canvas) {
        tetrominos.forEach(p -> p.draw(canvas));
    }

    private void createTerminos() {
        original.add(new Tetromino(new Paint() {{
            setColor(Color.RED);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.UP),
                new Piece(2, Direction.UP),
                new Piece(1, Direction.DOWN)
        ));

        original.add(new Tetromino(new Paint() {{
            setColor(Color.BLUE);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.UP),
                new Piece(1, Direction.DOWN),
                new Piece(1, Direction.DOWN_LEFT)
        ));

        original.add(new Tetromino(new Paint() {{
            setColor(Color.GRAY);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.UP),
                new Piece(1, Direction.DOWN),
                new Piece(1, Direction.DOWN_RIGHT)
        ));

        original.add(new Tetromino(new Paint() {{
            setColor(Color.GREEN);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.UP),
                new Piece(1, Direction.RIGHT),
                new Piece(1, Direction.UP_RIGHT)
        ));

        original.add(new Tetromino(new Paint() {{
            setColor(Color.YELLOW);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.UP),
                new Piece(1, Direction.LEFT),
                new Piece(1, Direction.UP_RIGHT)
        ));

        original.add(new Tetromino(new Paint() {{
            setColor(Color.MAGENTA);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.UP),
                new Piece(1, Direction.LEFT),
                new Piece(1, Direction.RIGHT)
        ));

        original.add(new Tetromino(new Paint() {{
            setColor(Color.LTGRAY);
        }}, tileSize,
                new Piece(0, Direction.DOWN),
                new Piece(1, Direction.RIGHT),
                new Piece(1, Direction.UP),
                new Piece(1, Direction.UP_LEFT)
        ));
    }

    public void userPressedLeft() {
        try {
            makeMove(p -> p.move(Direction.LEFT), p -> p.move(Direction.RIGHT), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void userPressedRight() {
        try {
            makeMove(p -> p.move(Direction.RIGHT), p -> p.move(Direction.LEFT), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void userPressedDown() {
        try {
            makeMove(p -> p.move(Direction.DOWN), p -> p.move(Direction.UP), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void userPressedRotate() {
        try {
            makeMove(Tetromino::rotate, Tetromino::rotateBack, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

