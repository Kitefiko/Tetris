package com.kitefiko.tetris.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kitefiko.tetris.R;
import com.kitefiko.tetris.item.ScoreItem;

import java.util.List;

public class ScoreAdapter extends ArrayAdapter<ScoreItem> {

    public ScoreAdapter(Context context, List<ScoreItem> scoreItems) {
        super(context, R.layout.view_score, scoreItems);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            convertView = inflater.inflate(R.layout.view_score, parent, false);
            holder = new ViewHolder();
            holder.dateTime = convertView.findViewById(R.id.date);
            holder.score = convertView.findViewById(R.id.score);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ScoreItem scoreItem = getItem(position);

        assert scoreItem != null;
        holder.dateTime.setText(DateFormat.format("dd/MM/yyyy hh:mm", scoreItem.dateTime).toString());
        holder.score.setText(String.valueOf(scoreItem.score));

        return convertView;
    }


    private class ViewHolder {
        private TextView dateTime;
        private TextView score;
    }
}
