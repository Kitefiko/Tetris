package com.kitefiko.tetris.item;

import java.io.Serializable;
import java.util.Date;

public class ScoreItem implements Serializable{
    public Date dateTime;
    public int score;
}
