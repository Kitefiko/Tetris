package com.kitefiko.tetris.activity;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kitefiko.tetris.R;
import com.kitefiko.tetris.adapter.ScoreAdapter;
import com.kitefiko.tetris.item.ScoreItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ScoreActivity extends AppCompatActivity {
    public static final String SCORE = "SCORE";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    ScoreAdapter scoreAdapter;
    List<ScoreItem> scoreItems;

    Gson gson = new Gson();

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_score);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        scoreItems = gson.fromJson(
                preferences.getString(SCORE, ""),
                new TypeToken<List<ScoreItem>>() {
                }.getType()
        );

        if (scoreItems != null) {
            scoreAdapter = new ScoreAdapter(this, scoreItems);
            ListView listView = findViewById(R.id.scoreList);
            listView.setAdapter(scoreAdapter);
        }
    }

    public void onClearClick(View view) {
        if(scoreItems != null){
           scoreAdapter.clear();
           scoreAdapter.notifyDataSetChanged();
        }
        scoreItems = null;
        editor = preferences.edit();
        editor.putString(SCORE, gson.toJson(scoreItems));
        editor.apply();
    }
}
