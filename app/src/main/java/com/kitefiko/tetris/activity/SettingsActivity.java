package com.kitefiko.tetris.activity;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

import com.kitefiko.tetris.R;

public class SettingsActivity extends AppCompatActivity {
    public static final String SOUND = "SETTINGS_SOUND";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        ((ToggleButton)findViewById(R.id.sound)).setChecked(preferences.getBoolean(SOUND, true));
    }

    public void onSoundClick(View view){
        editor = preferences.edit();
        if(((ToggleButton) view).isChecked()) {
            editor.putBoolean(SOUND,true);
        } else {
            editor.putBoolean(SOUND,false);
        }
        editor.apply();
    }
}
