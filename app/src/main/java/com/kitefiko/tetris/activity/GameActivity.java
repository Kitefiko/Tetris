package com.kitefiko.tetris.activity;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kitefiko.tetris.R;
import com.kitefiko.tetris.game.GameSurfaceView;
import com.kitefiko.tetris.game.OnScoreSet;
import com.kitefiko.tetris.item.ScoreItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GameActivity extends AppCompatActivity implements View.OnClickListener, OnScoreSet {
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    Gson gson = new Gson();

    GameSurfaceView gameSurfaceView;
    MediaPlayer mPlayer;
    TextView score;

    boolean sound;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        score = findViewById(R.id.score);

        gameSurfaceView = findViewById(R.id.gameSurfaceView);
        gameSurfaceView.setOnScoreSet(this, this);

        findViewById(R.id.left).setOnClickListener(this);
        findViewById(R.id.rotate).setOnClickListener(this);
        findViewById(R.id.down).setOnClickListener(this);
        findViewById(R.id.right).setOnClickListener(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        sound = preferences.getBoolean(SettingsActivity.SOUND, true);

        mPlayer = MediaPlayer.create(this, R.raw.tetris);
        mPlayer.setLooping(true);

        if (sound) {
            mPlayer.start();
        }
    }

    @Override
    protected void onResume() {
        if (sound) {
            mPlayer.start();
        }
        super.onResume();
        gameSurfaceView.playPause();
    }

    @Override
    public void onPause() {
        if (sound) {
            mPlayer.pause();
        }
        super.onPause();
        gameSurfaceView.playPause();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left) {
            gameSurfaceView.game.userPressedLeft();
        } else if (v.getId() == R.id.right) {
            gameSurfaceView.game.userPressedRight();
        } else if (v.getId() == R.id.rotate) {
            gameSurfaceView.game.userPressedRotate();
        } else if (v.getId() == R.id.down) {
            gameSurfaceView.game.userPressedDown();
        }
    }

    public void onPlayPauseClick(View view) {
        if (sound) {
            if (mPlayer.isPlaying()) {
                mPlayer.pause();
            } else {
                mPlayer.start();
            }
        }
        gameSurfaceView.playPause();
    }

    public void onSoundClick(View view) {
        if (mPlayer.isPlaying()) {
            mPlayer.pause();
            sound = false;
        } else {
            mPlayer.start();
            sound = true;
        }
    }

    @Override
    public void setScore(int score) {
        runOnUiThread(() -> this.score.setText(String.valueOf(score)));
    }

    @Override
    public void onGameOver(int score) {
        runOnUiThread(() -> {
            List<ScoreItem> scoreItems = gson.fromJson(
                    preferences.getString(ScoreActivity.SCORE, ""),
                    new TypeToken<List<ScoreItem>>() {
                    }.getType()
            );

            if(scoreItems == null) {
                scoreItems = new ArrayList<>();
            }

            ScoreItem scoreItem = new ScoreItem();
            scoreItem.score = score;
            scoreItem.dateTime = new Date();

            scoreItems.add(scoreItem);

            editor = preferences.edit();
            editor.putString(ScoreActivity.SCORE, gson.toJson(scoreItems));
            editor.apply();

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("GAME OVER");
            alertDialog.setMessage("Score: " + String.valueOf(score));
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> {
                        dialog.dismiss();
                        this.finish();
                        onBackPressed();
                    });
            alertDialog.show();
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
